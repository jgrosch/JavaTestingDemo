/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mooseriver.javatestingdemo;

/**
 *
 * @author jgrosch
 */
public class JavaTestingDemo {
    /**
     * 
     */
    private static final String VERSION = "0.1";
    
    /**
     * 
     */
    private static final String COPYRIGHT = "Copyright 2021 (c) Josef Grosch";
    
    /**
     * 
     * @return 
     */
    public String getVersion() {
        return this.VERSION;
    }
    
    /**
     * 
     * @return 
     */
    public String getCopyright() {
        return this.COPYRIGHT;
    }
}   // End of class JavaTestingDemo
